const express = require("express");
const router = express.Router();
const { getPaymentForm, chargeMoney, testCharge, test } = require("../controller/payment");

router.get("/", getPaymentForm);
router.post("/charge", chargeMoney);
router.get("/testcharge", testCharge);
router.get("/test", test);

module.exports = router;
