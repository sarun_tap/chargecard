// const publicKey = "pkey_test_5nq6zp6cpznxs4g7sih";
// const secretKey = "skey_test_5nq6zpxgac2k35y0wla";

const publicKey = process.env.OMISE_PUBLIC_KEY;
const secretKey = process.env.OMISE_SECRET_KEY;

const omiseConfig = {
  publicKey,
  secretKey,
};

const omise = require("omise")(omiseConfig);

module.exports.getPaymentForm = (req, res, next) => {
  res.render("payment/index");
};

module.exports.test = async (req, res, next) => {
  console.log(process.env.OMISE_PUBLIC_KEY);
  console.log(process.env.OMISE_SECRET_KEY);
  console.log(process.env.OMISE_PUBLIC_KEY === "pkey_test_5nq6zp6cpznxs4g7sih");
  console.log(process.env.OMISE_SECRET_KEY === "skey_test_5nq6zpxgac2k35y0wla");
  try {
    var cardDetails = {
      card: {
        name: "JOHN DOE",
        city: "Bangkok",
        postal_code: 10320,
        number: "4242424242424242",
        expiration_month: 2,
        expiration_year: 2022,
      },
    };

    const token = await omise.tokens.create(cardDetails);
    console.log(token);

    // const customer = await omise.customers.create({
    //   email: "john.doe@example.com",
    //   description: "John Doe (id: 30)",
    //   card: token.id,
    // });
    // const charge = await omise.charges.create({
    //   amount: 2000,
    //   currency: "thb",
    //   customer: customer.id,
    //   description: "this is the test",
    //   // card: req.body.omiseToken,
    // });
    res.send(token);
  } catch (err) {
    console.log("error");
    console.log(err);
  }
};

module.exports.chargeMoney = async (req, res, next) => {
  console.log(req.body.omiseToken);
  console.log(process.env.OMISE_SECRET_KEY);
  console.dir(omise.charges.create);
  try {
    // const customer = await omise.customers.create({
    //   email: "john.doe@example.com",
    //   description: "John Doe (id: 30)",
    //   card: req.body.omiseToken,
    // });
    const charge = await omise.charges.create({
      amount: 2000,
      currency: "thb",
      // customer: customer.id,
      description: "this is the test",
      card: req.body.omiseToken,
    });
    res.send(charge);
  } catch (err) {
    console.log("error");
    console.log(err);
  }
};

module.exports.testCharge = () => {
  var cardDetails = {
    card: {
      name: "JOHN DOE",
      city: "Bangkok",
      postal_code: 10320,
      number: "4242424242424242",
      expiration_month: 2,
      expiration_year: 2022,
    },
  };

  omise.tokens
    .create(cardDetails)
    .then(function (token) {
      console.log(token);
      return omise.customers.create({
        email: "john.doe@example.com",
        description: "John Doe (id: 30)",
        card: token.id,
      });
    })
    .then(function (customer) {
      console.log(customer);
      return omise.charges.create({
        amount: 10000,
        currency: "thb",
        customer: customer.id,
      });
    })
    .then(function (charge) {
      console.log(charge);
    })
    .error(function (err) {
      console.log(err);
    })
    .done();
};
