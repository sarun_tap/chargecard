const dotenv = require("dotenv");
const express = require("express");
const path = require("path");
const ejsMate = require("ejs-mate");


const paymentRoutes = require("./routes/payment");

dotenv.config({path: path.join(__dirname, "config/config.env")});


const app = express();
const PORT = process.env.PORT || 3000;

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.engine("ejs", ejsMate);

app.use(express.static(path.join(__dirname, "public")));

app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use("/payment", paymentRoutes);

app.listen(PORT, () => {
    console.log(`serving on port ${PORT} in ${process.env.NODE_ENV} mode`);
});